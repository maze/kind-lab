#!/usr/bin/env bash
which kind || (sudo true && sudo curl -Lo /usr/local/bin/kind https://kind.sigs.k8s.io/dl/v0.26.0/kind-linux-amd64 && sudo chmod +x /usr/local/bin/kind)
docker network create kind  # creating network in advanced prevents some host resolution bug
kind create cluster --config kind.yaml
