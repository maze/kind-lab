#!/usr/bin/env bash
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm upgrade --install --create-namespace -n monitoring kube-prom-stack prometheus-community/kube-prometheus-stack --set prometheusSpec.serviceMonitorSelectorNilUsesHelmValues=false --set grafana.additionalDataSources[0].name=Loki --set grafana.additionalDataSources[0].type=loki --set grafana.additionalDataSources[0].access=proxy --set grafana.additionalDataSources[0].url='http://loki-query-frontend-headless.logging:3100'
kubectl -n monitoring patch service kube-prom-stack-grafana -p '{"spec":{"type":"LoadBalancer"}}'
kubectl -n monitoring get secret kube-prom-stack-grafana -o jsonpath="{.data.admin-password}" | base64 -d; echo  # Grafana admin password
