#!/usr/bin/env bash
helm repo add argo https://argoproj.github.io/argo-helm
helm upgrade --install --create-namespace -n argocd argocd argo/argo-cd --set server.service.type=LoadBalancer
kubectl -n argocd wait --for=condition=Ready pod -l app.kubernetes.io/name=argocd-server --timeout=90s
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d; echo  # Argo CD admin password
